# Create a build stage from the current nomad dev_python image to copy the nomad-lab
# distribution
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-fair/dev_python:develop AS dev_python

FROM jupyter/datascience-notebook:lab-3.6.2

USER root
RUN apt-get update \
 && apt-get install --yes --quiet --no-install-recommends \
      libmagic-dev \
 && rm -rf /var/lib/apt/lists/*
USER 1000

RUN pip install --no-cache-dir \
    --pre --extra-index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple \
        'nomad-lab==1.3.1'
RUN pip install hzb-combinatorial-libraries@git+https://github.com/nomad-hzb/hzb-combinatorial-libraries-plugin.git --index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple
RUN pip install voila
RUN pip install ipyaggrid
RUN pip install ipysheet
RUN pip install ipydatagrid
RUN pip install jupyter-flex
RUN pip install baybe
RUN pip install pyvis